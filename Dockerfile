FROM ubuntu:20.04 as base

ARG DEBIAN_FRONTEND=noninteractive

WORKDIR /build

ADD files files
ADD install.sh .
RUN ./install.sh && rm -rf /var/lib/apt/lists/*

COPY start-container /usr/local/bin/start-container
RUN chmod +x /usr/local/bin/start-container
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

WORKDIR /data
RUN rm -rf /build
COPY config_generator.py ./
COPY conf_stub conf_stub

ENTRYPOINT ["start-container"]