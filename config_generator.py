#!/usr/bin/env python3.8

import argparse
import ipaddress
import os
import socket
from urllib.request import urlopen

parser = argparse.ArgumentParser("config_generator")
parser.add_argument("dest_dir", help="Destination directory is required")

args = parser.parse_args()

AST_CONF_DIR = args.dest_dir + "/asterisk"
OTHER_CONF_DIR = args.dest_dir + "/other"

APP_PUBLIC_DEPLOYMENT = "APP_PUBLIC_DEPLOYMENT"
DOCKER_INTERNAL_HOSTNAME = "DOCKER_INTERNAL_HOSTNAME"
AST_EXTERNAL_IP = "AST_EXTERNAL_IP"
AST_PRIVATE_IP = "AST_PRIVATE_IP"
# Database
DB_HOST = "DB_HOST"
DB_PORT = "DB_PORT"
DB_DATABASE = "DB_DATABASE"
DB_USERNAME = "DB_USERNAME"
DB_PASSWORD = "DB_PASSWORD"
# Asterisk Manager
AST_MANAGER_BINDADDR = "AST_MANAGER_BINDADDR"
AST_MANAGER_PORT = "AST_MANAGER_PORT"
AST_MANAGER_USERNAME = "AST_MANAGER_USERNAME"
AST_MANAGER_PASSWORD = "AST_MANAGER_PASSWORD"
# Asterisk Ari
AST_ARI_USERNAME = "AST_ARI_USERNAME"
AST_ARI_PASSWORD = "AST_ARI_PASSWORD"
# Asterisk HTTP
AST_HTTP_BINDADDR = "AST_HTTP_BINDADDR"
# Asterisk PJSIP Configuration
AST_PJSIP_UDP_ENABLED = "AST_PJSIP_UDP_ENABLED"
AST_PJSIP_UDP_TRANSPORT_NAME = "AST_PJSIP_UDP_TRANSPORT_NAME"
AST_PJSIP_UDP_BIND = "AST_PJSIP_UDP_BIND"
AST_PJSIP_WSS_ENABLED = "AST_PJSIP_WSS_ENABLED"
AST_PJSIP_WSS_TRANSPORT_NAME = "AST_PJSIP_WSS_TRANSPORT_NAME"
AST_PJSIP_WSS_BIND = "AST_PJSIP_WSS_BIND"
AST_PJSIP_WSS_PROTOCOL = "AST_PJSIP_WSS_PROTOCOL"
AST_PJSIP_TCP_ENABLED = "AST_PJSIP_TCP_ENABLED"
AST_PJSIP_TCP_TRANSPORT_NAME = "AST_PJSIP_TCP_TRANSPORT_NAME"
AST_PJSIP_TCP_BIND = "AST_PJSIP_TCP_BIND"
AST_PJSIP_TLS_ENABLED = "AST_PJSIP_TLS_ENABLED"
AST_PJSIP_TLS_TRANSPORT_NAME = "AST_PJSIP_TLS_TRANSPORT_NAME"
AST_PJSIP_TLS_BIND = "AST_PJSIP_TLS_BIND"
# Asterisk ODBC
AST_ODBC_DSN_NAME = "AST_ODBC_DSN_NAME"
# Asterisk Trunk
AST_SIP_TRUNK_REGISTRATION_ENABLED = "AST_SIP_TRUNK_REGISTRATION_ENABLED"
AST_SIP_TRUNK_HOST = "AST_SIP_TRUNK_HOST"
AST_SIP_TRUNK_USERNAME = "AST_SIP_TRUNK_USERNAME"
AST_SIP_TRUNK_PASSWORD = "AST_SIP_TRUNK_PASSWORD"

# Asterisk SIP
AST_SIP_TRUNK_CONTEXT = "AST_SIP_TRUNK_CONTEXT"
# Asterisk Stasis
AST_STASIS_APP_NAME = "AST_STASIS_APP_NAME"

ENV_DEFS = {
    APP_PUBLIC_DEPLOYMENT: {"type": "bool", "default": False},
    DOCKER_INTERNAL_HOSTNAME: {"type": "string", "default": "host.docker.internal"},
    AST_EXTERNAL_IP: {"type": "string", "default_resolver": "getExternalIp"},
    AST_PRIVATE_IP: {"type": "string", "default_resolver": "getPrivateIp"},

    # Database
    DB_HOST: {"type": "string", "host.docker.internal"},
    DB_PORT: {"type": "string"},
    DB_DATABASE: {"type": "string"},
    DB_USERNAME: {"type": "string"},
    DB_PASSWORD: {"type": "string"},

    # Asterisk Manager
    AST_MANAGER_BINDADDR: {"type": "string", "default": "0.0.0.0"},
    AST_MANAGER_PORT: {"type": "string", "default": "5038"},
    AST_MANAGER_USERNAME: {"type": "string"},
    AST_MANAGER_PASSWORD: {"type": "string"},

    # Asterisk Ari
    AST_ARI_USERNAME: {"type": "string", "default": "asterisk"},
    AST_ARI_PASSWORD: {"type": "string", "default": "asterisk_ari_password"},

    # Asterisk HTTP
    AST_HTTP_BINDADDR: {"type": "string", "default": "0.0.0.0"},

    # Asterisk PJSIP Configuration
    AST_PJSIP_UDP_ENABLED: {"type": "bool", "default": True},
    AST_PJSIP_UDP_TRANSPORT_NAME: {"type": "string", "default": "transport-udp"},
    AST_PJSIP_UDP_BIND: {"type": "string", "default": "0.0.0.0:5060"},

    AST_PJSIP_WSS_ENABLED: {"type": "bool", "default": True},
    AST_PJSIP_WSS_TRANSPORT_NAME: {"type": "string", "default": "transport-wss"},
    AST_PJSIP_WSS_BIND: {"type": "string", "default": "0.0.0.0:8088"},
    AST_PJSIP_WSS_PROTOCOL: {"type": "string", "default": "ws"},

    AST_PJSIP_TCP_ENABLED: {"type": "bool", "default": False},
    AST_PJSIP_TCP_TRANSPORT_NAME: {"type": "string", "default": "transport-tcp"},
    AST_PJSIP_TCP_BIND: {"type": "string", "default": "0.0.0.0:5062"},

    AST_PJSIP_TLS_ENABLED: {"type": "bool", "default": False},
    AST_PJSIP_TLS_TRANSPORT_NAME: {"type": "string", "default": "transport-tls"},
    AST_PJSIP_TLS_BIND: {"type": "string", "default": "0.0.0.0:5063"},

    # Asterisk ODBC
    AST_ODBC_DSN_NAME: {"type": "string", "default": "smartcomm_callcenter"},

    # Asterisk Trunk
    AST_SIP_TRUNK_REGISTRATION_ENABLED: {"type": "bool", "default": False},
    AST_SIP_TRUNK_HOST: {"type": "string"},
    AST_SIP_TRUNK_USERNAME: {"type": "string"},
    AST_SIP_TRUNK_PASSWORD: {"type": "string"},
    # Asterisk SIP
    AST_SIP_TRUNK_CONTEXT: {"type": "string"},
    # Asterisk Stasis
    AST_STASIS_APP_NAME: {"type": "string"},
}

AST_CONFIG_FILES = {
    'ari.conf': None,
    'asterisk.conf': None,
    'cdr.conf': None,
    'cel.conf': None,
    'extconfig.conf': None,
    'extensions.conf': "ast_extensions_config_transformer",
    'http.conf': None,
    'logger.conf': None,
    'manager.conf': None,
    'modules.conf': None,
    'pjsip.conf': "ast_pjsip_config_transformer",
    'res_odbc.conf': None,
    "rtp.conf": "ast_rtp_config_transformer",
    "sip.conf": "ast_sip_config_transformer",
    'sorcery.conf': None
}

OTHRE_CONFIG_FILES = {
    "odbc.ini": None,
    "odbcinst.ini": None
}


def getBooleanEnv(key, default=True):
    return True if os.getenv(key) in [True, "TRUE", "true", "YES", "yes"] else default


def getStringEnv(key, default=None):
    return os.getenv(key) if os.getenv(key) is not None and os.getenv(key).strip() != "" else default


def getExternalIp():
    if getBooleanEnv("APP_PUBLIC_DEPLOYMENT", False):
        for site in ["https://ident.me", "https://checkip.amazonaws.com", "http://ifconfig.co", "http://ifconfig.me"]:
            try:
                ip = ipaddress.ip_address(urlopen(site, timeout=20).read().decode('utf8'))
                return ip
            except Exception as e:
                pass
        raise Exception("Faild to identify external ip")
    else:
        return None


def getPrivateIp():
    # Ref: https://stackoverflow.com/a/30990617/2317535
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]


ENVS = {}

for env_name in ENV_DEFS:
    env_prop = ENV_DEFS[env_name]
    default = env_prop["default"] if "default" in env_prop else None
    if default is None:
        resolver = env_prop["default_resolver"] if "default_resolver" in env_prop else None
        default = locals()[resolver]() if resolver is not None else None

    if env_prop["type"] == "bool":
        ENVS[env_name] = getBooleanEnv(env_name, default) if default is not None else getBooleanEnv(env_name)
    elif env_prop["type"] == "string":
        ENVS[env_name] = getStringEnv(env_name, default) if default is not None else getStringEnv(env_name)


def get_env(env_name):
    if env_name not in ENVS:
        raise Exception("{} is not valid environment variable".format(env_name))
    return ENVS[env_name]


def ast_rtp_config_transformer(config_data):
    sec_general_part_ice_permit_rules = f"""
icesupport => yes
ice_deny => 0.0.0.0/0
ice_deny => ::/0
ice_permit => {get_env(AST_PRIVATE_IP)}
ice_permit => {get_env(AST_EXTERNAL_IP)}
"""
    sec_ice_host_candidates_data = f"""
[ice_host_candidates]
{get_env(AST_PRIVATE_IP)} => {get_env(AST_EXTERNAL_IP)}
"""
    return config_data + sec_general_part_ice_permit_rules + sec_ice_host_candidates_data


def ast_sip_config_transformer(config_data):
    trunk_register = f"""
register => {get_env(AST_SIP_TRUNK_USERNAME)}:{get_env(AST_SIP_TRUNK_PASSWORD)}@{get_env(AST_SIP_TRUNK_HOST)}
"""

    trunk_section = f"""
[{get_env(AST_SIP_TRUNK_USERNAME)}]
type => friend
secret => {get_env(AST_SIP_TRUNK_PASSWORD)}
username => {get_env(AST_SIP_TRUNK_USERNAME)}
host => {get_env(AST_SIP_TRUNK_HOST)}
dtmfmode => rfc2833
canreinvite => yes
disallow => all
allow => G711
allow => ulaw
allow => gsm
allow => alaw 
fromdomain => {get_env(AST_SIP_TRUNK_HOST)}
context => {get_env(AST_SIP_TRUNK_CONTEXT)}
"""

    if get_env(AST_SIP_TRUNK_REGISTRATION_ENABLED):
        config_data = config_data + trunk_register + trunk_section
    return config_data


def ast_extensions_config_transformer(config_data):
    section_from_trunk = f"""

[{get_env(AST_SIP_TRUNK_CONTEXT)}]
exten => s,1,Log(NOTICE, INCOMING from {get_env(AST_SIP_TRUNK_USERNAME)} trunk, caller number: ${{CALLERID(number)}})
same  =>   n,Set(INCOMING=1)
same  =>    n,Set(TRUNK={get_env(AST_SIP_TRUNK_USERNAME)})
same  =>   n,Set(CALLID=${{CALLERID(dnid)}})
same  =>   n,Set(CALLER=${{CALLERID(number)}})
same  =>   n,Stasis({get_env(AST_STASIS_APP_NAME)})
same  =>   n,Hangup()

[one-2-1-calling]
exten =>   s,1,Log(NOTICE, calling  recepient: ${{CALLERID(dnid)}}, caller number: ${{CALLERID(number)}})
same  =>   n,Set(CALLID=${{CALLERID(dnid)}})
same  =>   n,Set(CALLER=${{CALLERID(number)}})
same  =>   n,Stasis({get_env(AST_STASIS_APP_NAME)})
same =>    n,Hangup()

[agent-ctx]
exten => _.,1,Log(NOTICE, Call from ${{CALLERID(dnid)}} to ${{EXTEN}})
same  =>   n,Set(INCOMING=1)
same => n,Set(AGC(rx)=32000)
same => n,Set(AGC(tx)=32000)
same => n,Set(DENOISE(rx)=on)
same => n,Set(DENOISE(tx)=on)
same => n,Goto(one-2-1-calling,s,1)
same => n,Hangup()


[test-hello-world]
exten => _.,1,Log(NOTICE, Call from ${{CALLERID(dnid)}} to ${{EXTEN}})
same  => n,Ringing()
same  => n,Answer()
same => n,Set(AGC(rx)=32000)
same => n,Set(AGC(tx)=32000)
same => n,Set(DENOISE(rx)=on)
same => n,Set(DENOISE(tx)=on)
same => n,Wait(1)
same => n,Playback(hello-world)
same => n,Wait(1)
same => n,Playback(hello-world)
same => n,Wait(1)
same => n,Playback(hello-world)
same => n,Hangup()

[test-echo]
exten => _.,1,Log(NOTICE, Call from ${{CALLERID(dnid)}} to ${{EXTEN}})
same  => n,Ringing()
same => n,Wait(2) 
same => n,Answer()
same => n,Set(AGC(rx)=32000)
same => n,Set(AGC(tx)=32000)
same => n,Set(DENOISE(rx)=on)
same => n,Set(DENOISE(tx)=on)
same => n,Playback(conf-now-recording)
same => n,Record(en/test-echo-recording.gsm, 0, 5)
same => n,Wait(1)
same => n,Playback(test-echo-recording)
same => n,Hangup()
"""
    return config_data + section_from_trunk


def ast_pjsip_config_transformer(config_data):
    section_udp = f"""

[{get_env(AST_PJSIP_UDP_TRANSPORT_NAME)}]
type => transport
protocol => udp
bind => {get_env(AST_PJSIP_UDP_BIND)}
"""

    section_wss = f"""

[{get_env(AST_PJSIP_WSS_TRANSPORT_NAME)}]
type => transport
protocol => {get_env(AST_PJSIP_WSS_PROTOCOL)}
bind => {get_env(AST_PJSIP_WSS_BIND)}
local_net => {get_env(AST_PRIVATE_IP)}/24
external_media_address => {get_env(AST_EXTERNAL_IP)}
external_signaling_address => {get_env(AST_EXTERNAL_IP)}
"""

    section_tcp = f"""
    
[{get_env(AST_PJSIP_TCP_TRANSPORT_NAME)}]
type => transport
protocol => tcp
bind => {get_env(AST_PJSIP_TCP_BIND)}
local_net => {get_env(AST_PRIVATE_IP)}/24
external_media_address => {get_env(AST_EXTERNAL_IP)}
external_signaling_address => {get_env(AST_EXTERNAL_IP)}
"""

    section_tls = f"""
    
[{get_env(AST_PJSIP_TLS_TRANSPORT_NAME)}]
type => transport
protocol => tls
bind => {get_env(AST_PJSIP_TLS_BIND)}
;cert_file =>
;priv_key_file =>
;method => sslv23
local_net => {get_env(AST_PRIVATE_IP)}/24
external_media_address => {get_env(AST_EXTERNAL_IP)}
external_signaling_address => {get_env(AST_EXTERNAL_IP)}
"""

    if get_env(AST_PJSIP_UDP_ENABLED):
        config_data = config_data + section_udp
    if get_env(AST_PJSIP_WSS_ENABLED):
        config_data = config_data + section_wss
    if get_env(AST_PJSIP_TCP_ENABLED):
        config_data = config_data + section_tcp
    if get_env(AST_PJSIP_TLS_ENABLED):
        config_data = config_data + section_tls
    return config_data


def get_filled_ast_config_file_contents(config_file_name, transformer=None, extra={}):
    file_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "conf_stub", config_file_name)
    if not os.path.exists(file_path):
        raise Exception("{} file is not found!".format(file_path))

    envs = {**ENVS, **extra}
    with open(file_path) as file:
        contents = file.read()
        try:
            config_data = contents.format(**envs)
            if transformer is not None:
                config_data = globals()[transformer](config_data)
            return config_data
        except KeyError as e:
            raise Exception("{} key not provided in {} file!".format(str(e), config_file_name))


def write_config_file(config_file_name, transformer, out_dir):
    contents = get_filled_ast_config_file_contents(config_file_name, transformer)
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    with open(out_dir + "/" + config_file_name, "w") as w:
        w.write(contents)


if __name__ == "__main__":
    for ast_conf_file in AST_CONFIG_FILES:
        write_config_file(ast_conf_file, AST_CONFIG_FILES[ast_conf_file], AST_CONF_DIR)

    for other_conf_file in OTHRE_CONFIG_FILES:
        write_config_file(other_conf_file, OTHRE_CONFIG_FILES[other_conf_file], OTHER_CONF_DIR)
