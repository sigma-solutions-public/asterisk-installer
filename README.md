## Description

This project builds a docker container that has asterisk and odbc in it. This container uses host network, meaning that container’s network stack is not isolated from the Docker host (the container shares the host’s networking namespace), and the container does not get its own IP-address allocated.


## Installation

Ensure that docker and docker-compose is installed on the host machine.

Create a `.env` file from `.env.example` file. 

For local development purpose, update `HOST_PUBLIC_IP` and `HOST_PRIVATE_IP` with host machine ip. Create a dabase and update database config in the `.env` file. 

For production, update `TRUNK` information and `TLS` information.


Then run:

```bash
$ docker-compose up
```

When this command is run at the first time, the image is build hence it will take time.  Later times, the container will run immediately. 

To list docker containers:
```bash
$ docker ls
```

To stop this docker container:

```bash
$ docker stop asterisk
```

To enter into this docker container:
```bash
$ docker exec -it asterisk bash
```


If `.env` value is changed, then run `docker-compose build` and `docker-compose up` to apply the changes in the container.



### How to use gen_config
Use `gen_config.sh` to generate config file from .env

     $ ./gen_config.sh .env out