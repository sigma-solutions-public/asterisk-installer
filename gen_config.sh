#!/usr/bin/env bash

set -euo pipefail

env_file=''
verbose='false'

display_help() {
  echo "Usage: $0 ENV_FILE DEST_DIR"
  echo "Generate asterisk and odbc config file in DEST_DIR/asterisk and DEST_DIR/odbc directory"
}

if [ $# -gt 0 ]; then
  if [ "$1" == "help" ] || [ "$1" == "-h" ] || [ "$1" == "-help" ] || [ "$1" == "--help" ]; then
      display_help
      exit 0
  fi
fi
if [ $# -lt 2 ]; then
  if [ $# -gt 0 ]; then
    echo "Insufficient parameters!!!"
    echo  ""
  fi
  display_help
  exit 1
fi


if [ ! -f $1 ]; then
    echo "$1 file is not found!"
    exit 1
fi

export $(grep -v '^#' $1 | xargs)

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )";
$SCRIPT_DIR/config_generator.py $2