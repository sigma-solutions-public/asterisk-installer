#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )";

apt-get update && apt-get install -y --no-install-recommends --no-install-suggests  \
    software-properties-common \
    supervisor \
    autoconf \
    binutils-dev \
    build-essential \
    curl \
    file \
    vim \
    libcurl4-openssl-dev \
    libedit-dev \
    libgsm1-dev \
    libjansson-dev \
    libogg-dev \
    libpopt-dev \
    libresample1-dev \
    libspandsp-dev \
    libspeex-dev \
    libspeexdsp-dev \
    libsqlite3-dev \
    libsrtp2-1 \
    libsrtp2-dev \
    libssl-dev \
    libvorbis-dev \
    libxml2-dev \
    libxslt1-dev \
    portaudio19-dev \
    unixodbc \
    unixodbc-dev \
    odbcinst \
    uuid \
    uuid-dev \
    xmlstarlet \
    subversion


pushd /tmp/
tar -xzf $SCRIPT_DIR/files/libsrtp-2.3.0.tar.gz -C .
tar -xzf $SCRIPT_DIR/files/mysql-connector-odbc-8.0.24-linux-glibc2.12-x86-64bit.tar.gz -C .
tar -xzf $SCRIPT_DIR/files/asterisk-18.4.0.tar.gz -C .

# Install libsrtp
pushd libsrtp-*
./configure --with-ssl && \
    make && \
    make install
popd
# mysql connector
pushd  mysql-connector-odbc-*
cp -r bin/* /usr/local/bin && \
    cp -r lib/* /usr/local/lib && \
    ./bin/myodbc-installer -a -d -n "MySQL ODBC 8.0 Driver" -t "Driver=/usr/local/lib/libmyodbc8w.so" && \
    ./bin/myodbc-installer -a -d -n "MySQL ODBC 8.0" -t "Driver=/usr/local/lib/libmyodbc8a.so"
popd
pushd asterisk-*

contrib/scripts/install_prereq install && \
    ./configure --with-pjproject --with-ssl --with-srtp --disable-video && \
    make && \
    make install
popd

rm -rf libsrtp-*
rm -rf mysql-connector-odbc-*
rm -rf asterisk-*
popd